import React from "react";

const referral = `?utm_source=${
  process.env.REACT_APP_UNSPLASH_APP_NAME
}&utm_medium=referral`;

export default class ImageCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = { spans: 0 };
    this.boxRef = React.createRef();
  }

  setSpans = () => {
    const height = this.boxRef.current.parentNode.clientHeight;
    const spans = Math.ceil(height / 15);

    this.setState({ spans });
  };

  componentDidMount = () => {
    this.boxRef.current.addEventListener("load", this.setSpans);
  };

  render() {
    const img = this.props.img;
    return (
      <div style={{ gridRowEnd: `span ${this.state.spans}` }}>
        <div className="ui segment">
          <img
            className="ui image"
            ref={this.boxRef}
            src={img.urls.regular}
            alt={img.description}
          />
          <div className="description">
            Photo by{" "}
            <a href={img.user.links.html + referral}>{img.user.name}</a> on{" "}
            <a href={"https://unsplash.com/" + referral}>Unsplash</a>
          </div>
        </div>
      </div>
    );
  }
}
