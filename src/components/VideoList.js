import React from "react";
import youtube from "../apis/youtube";
import withContentRetriever from "./withContentRetriever";
import VideoItem from "./VideoItem";
import { List } from "semantic-ui-react";

const VideoList = ({ data }) => {
  const content = data.map(v => {
    return <VideoItem key={v.id.videoId} video={v} />;
  });
  return <List relaxed>{content}</List>;
};

export default withContentRetriever(VideoList, async term => {
  const res = await youtube.get("/search", {
    params: {
      q: term,
      type: "video"
    }
  });
  return res.data.items;
});
