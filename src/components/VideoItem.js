import React from "react";
import { Item, Image } from "semantic-ui-react";

export default function VideoItem({ video, size = "medium" }) {
  return (
    <Item>
      <Image
        src={video.snippet.thumbnails[size].url}
        alt={video.snippet.description}
      />
      <Item.Content>
        <Item.Header>{video.snippet.title}</Item.Header>
      </Item.Content>
    </Item>
  );
}
