import "./ImageList.css";
import ImageCard from "./ImageCard";
import React from "react";
import unsplash from "../apis/unsplash";
import withContentRetriever from "./withContentRetriever";

const ImageList = props => {
  const images = props.data.map(img => {
    return <ImageCard key={img.id} img={img} />;
  });
  return <div className="image-list">{images}</div>;
};

export default withContentRetriever(ImageList, async term => {
  const res = await unsplash.get("/search/photos", {
    params: { query: term }
  });
  return res.data.results;
});
