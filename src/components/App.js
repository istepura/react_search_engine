import React, { Component } from "react";
import { Container, Segment } from "semantic-ui-react";
import SearchBar from "./SearchBar";
import { Tab } from "semantic-ui-react";
import VideoList from "./VideoList";
import ImageList from "./ImageList";

export default class App extends Component {
  panes = [
    {
      menuItem: "Images",
      render: () => (
        <Tab.Pane attached={false}>
          <ImageList term={this.state.term} />
        </Tab.Pane>
      )
    },
    {
      menuItem: "Videos",
      render: () => (
        <Tab.Pane attached={false}>
          <VideoList term={this.state.term} />
        </Tab.Pane>
      )
    }
  ];
  state = { term: "" };
  onSearchSubmit = term => {
    this.setState({ term });
  };

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  render() {
    return (
      <Container>
        <Segment>
          <SearchBar onSubmit={this.onSearchSubmit} />
        </Segment>
        <Tab menu={{ text: true }} panes={this.panes} />
      </Container>
    );
  }
}
