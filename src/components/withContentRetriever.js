import React from "react";

export default function withContentRetriever(WrappedComponent, fetchData) {
  return class extends React.Component {
    constructor(props) {
      super(props);
      this.state = { results: [] };
    }

    fetchContent = async term => {
      if (term) {
        const results = await fetchData(term);

        this.setState({ results });
      }
    };

    componentDidMount = () => {
      this.fetchContent(this.props.term);
    };

    componentDidUpdate = ({ term }) => {
      if (term !== this.props.term) {
        this.fetchContent(this.props.term);
      }
    };

    render() {
      return <WrappedComponent data={this.state.results} {...this.props} />;
    }
  };
}
