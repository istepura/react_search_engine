import React, { Component } from "react";
import { Form, Input } from "semantic-ui-react";

export default class SearchBar extends Component {
  state = { term: "" };

  onFormSubmit = event => {
    event.preventDefault();
    this.props.onSubmit(this.state.term);
  };

  render() {
    return (
      <Form onSubmit={this.onFormSubmit}>
        <Form.Field>
          <Input
            placeholder="Search..."
            value={this.state.term}
            onChange={e => this.setState({ term: e.target.value })}
          />
        </Form.Field>
      </Form>
    );
  }
}
